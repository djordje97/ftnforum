using System;
using System.IO;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Runtime;
using FTNForum.Data.Entity;
using FTNForum.Data.Util;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace FTNForum.Repositories
{
    public class ForumDbContext : DbContext
    {

        public ForumDbContext(DbContextOptions<ForumDbContext> options)
            : base(options)
        { }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(@Directory.GetCurrentDirectory() + "/../FTNForum.Web/appsettings.json").Build();
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
            optionsBuilder.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasData(
                new Role() { Id = 1, Name = "User", CreatedAt = DateTime.Now },
                new Role() { Id = 2, Name = "Admin", CreatedAt = DateTime.Now }
            );
            modelBuilder.Entity<User>().HasData(
               new User() { Id = 1, FirstName = "Marko", Surname = "Markovic", Username = "marko", Email = "marko@gmail.com", IsActive = true, Password = "AQAAAAEAACcQAAAAEHzn1jsKZ+C5fkIlE5cAUDJhtxOVj1be+4qmCNu0w2LIbC+EJY1enxhHHZgG56Na1Q==", CreatedAt = DateTime.Now, RoleId = 2 },
               new User() { Id = 2, FirstName = "Darko", Surname = "Radic", Username = "darko", Email = "darko@gmail.com", IsActive = true, Password = "AQAAAAEAACcQAAAAEHzn1jsKZ+C5fkIlE5cAUDJhtxOVj1be+4qmCNu0w2LIbC+EJY1enxhHHZgG56Na1Q==", CreatedAt = DateTime.Now, RoleId = 1 }
               );

            modelBuilder.Entity<Forum>().HasData(
               new Forum() { Id = 1, Title = "C#", Description = "Object oriented programming language", ImageUrl = "", CreatedAt = DateTime.Now },
               new Forum() { Id = 2, Title = "JavaScript", Description = "Programming language for web", ImageUrl = "", CreatedAt = DateTime.Now }
           );

            modelBuilder.Entity<Post>().HasData(
              new Post() { Id = 1, Title = "Lambda expresion", Content = "Can anyone explain me how lambda works", UserId = 2, ForumId = 1, CreatedAt = DateTime.Now },
              new Post() { Id = 2, Title = "Let or Var", Content = "What should I use for declaring variables, let or var ?", UserId = 1, ForumId = 2, CreatedAt = DateTime.Now }
          );
        }

        public DbSet<Forum> Forums { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Reply> Replies { get; set; }
        public DbSet<Like> Likes { get; set; }
    }
}