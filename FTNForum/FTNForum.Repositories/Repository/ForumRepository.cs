using FTNForum.Data.Entity;
using FTNForum.Repositories.Interface;

namespace FTNForum.Repositories.Repository
{
    public class ForumRepository:Repository<Forum>,IForumRepository
    {
        public ForumRepository(ForumDbContext dbContext):base(dbContext)
        {
            
        }
    }
}