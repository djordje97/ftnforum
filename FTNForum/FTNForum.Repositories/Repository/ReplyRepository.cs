using FTNForum.Data.Entity;
using FTNForum.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace FTNForum.Repositories.Repository
{
    public class ReplyRepository:Repository<Reply>,IReplyRepository
    {
        public ReplyRepository(ForumDbContext dbContext):base(dbContext)
        {
            
        }

        public List<Reply> GetByPostId(long postId)
        // {
        //     var list = _dbContext.Replies.Include(x => x.Post).Include(x => x.User)
        //         .ThenInclude(x => x.Role)
        {       var list = _dbContext.Replies.Where(x => x.PostId == postId).ToList();
            return list;
        }
    }
}