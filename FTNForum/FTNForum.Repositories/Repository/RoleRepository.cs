using FTNForum.Data.Entity;
using FTNForum.Repositories.Interface;

namespace FTNForum.Repositories.Repository
{
    public class RoleRepository:Repository<Role>,IRoleRepository
    {
        public RoleRepository(ForumDbContext dbContext):base(dbContext)
        {
            
        }
    }
}