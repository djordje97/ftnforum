using System.Collections.Generic;
using System.Linq;
using FTNForum.Data.Entity;
using FTNForum.Repositories.Interface;
using Microsoft.EntityFrameworkCore;

namespace FTNForum.Repositories.Repository
{
    public class PostRepository:Repository<Post>,IPostRepository
    {
        public PostRepository(ForumDbContext dbContext):base(dbContext)
        {
            
        }

        public List<Post> GetByForumId(long forumId)
        {
            var postList = _dbContext.Posts
                .Where(post => post.ForumId == forumId).ToList();
            return postList;
        }

        public override Post GetById(long id)
        {
            var entity = _dbContext.Posts.Where(entity => entity.Id == id).FirstOrDefault();
            return entity;
        }

        public override List<Post> GetAll()
        {
            var postList = _dbContext.Posts
                .ToList();
            return postList;
        }
        
        public  List<Post> SearchPosts(string query)
        {
            var postList = _dbContext.Posts
                .Where(p => p.Title.ToLower().Contains(query.ToLower()) ||
                            p.Content.ToLower().Contains(query.ToLower())).ToList();
            return postList;
        }
    }
}