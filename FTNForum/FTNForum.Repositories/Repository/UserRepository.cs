
using System.Linq;
using FTNForum.Data.Entity;
using FTNForum.Repositories.Interface;
using Microsoft.EntityFrameworkCore;

namespace FTNForum.Repositories.Repository
{
    public class UserRepository:Repository<User>,IUserRepository
    {
        public UserRepository(ForumDbContext dbContext):base(dbContext)
        {
           
        }

        public User GetByUsername(string username)
        {
           return _dbContext.Users.Where(x => x.Username == username).FirstOrDefault();
        }

        public override User GetById(long id)
        {
            return _dbContext.Users.Where(x => x.Id == id).FirstOrDefault();
        }

        public string GetUserPassword(long id)
        {
            var password = _dbContext.Users.Where(x => x.Id == id).Select(x => x.Password).FirstOrDefault();
            return password;
        }
    }
}