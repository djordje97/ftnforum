using System.Collections.Generic;
using System.Linq;
using FTNForum.Repositories.Interface;
using Microsoft.EntityFrameworkCore;

namespace FTNForum.Repositories.Repository
{
    public class Repository<T>:IRepository<T> where T:class
    {
        protected  ForumDbContext _dbContext { get; set; }

        public Repository(ForumDbContext dbContext)
        {
            _dbContext = dbContext;
        }


        public virtual List<T> GetAll()
        {
            return _dbContext.Set<T>().ToList();
        }

        public virtual T GetById(long id)
        {
            return _dbContext.Set<T>().Find(id);
        }

        public virtual T Add(T entity)
        {
            _dbContext.Set<T>().Add(entity);
            return entity;
        }

        public virtual T Update(T entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public virtual void Delete(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
        }
    }
}