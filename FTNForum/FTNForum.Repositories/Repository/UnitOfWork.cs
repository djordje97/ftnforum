using FTNForum.Data.Entity;
using FTNForum.Repositories.Interface;

namespace FTNForum.Repositories.Repository
{
    public class UnitOfWork:IUnitOfWork
    {
        public IForumRepository Forums { get; }
        public ILikeRepository Likes { get; }
        public IPostRepository Posts { get; }
        public IReplyRepository Replies { get; }
        private IUserRepository _users;

        public IUserRepository Users
        {
            get
            {
                if(_users == null)
                    _users = new UserRepository(_dbContext);
                return _users;
            }
        }

        public IRoleRepository Roles { get; }

        private readonly ForumDbContext _dbContext;
        public int Complete()
        {
            return _dbContext.SaveChanges();
        }

        public UnitOfWork(IForumRepository forums,ILikeRepository likes,IPostRepository posts,IReplyRepository replies,IRoleRepository roles,ForumDbContext dbContext)
        {
            _dbContext = dbContext;
            Forums = forums;
            Likes = likes;
            Posts = posts;
            Replies = replies;
            Roles = roles;
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}