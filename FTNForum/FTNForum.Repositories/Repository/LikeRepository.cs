using System.Linq;
using FTNForum.Data.Entity;
using FTNForum.Repositories.Interface;
using Microsoft.EntityFrameworkCore;

namespace FTNForum.Repositories.Repository
{
    public class LikeRepository:Repository<Like>,ILikeRepository
    {
        public LikeRepository(ForumDbContext dbContext):base(dbContext)
        {
            
        }

        public bool GetLikeByUserAndReply(long userId, long replyId)
        {
            var likeExists = false;
            var like = _dbContext.Likes.AsNoTracking()
                .FirstOrDefault(x => x.UserId == userId && x.ReplyId == replyId);
            if (like != null)
                likeExists = true;
            return likeExists;
        }
    }
}