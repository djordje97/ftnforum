using System.Collections.Generic;
using FTNForum.Data.Entity;

namespace FTNForum.Repositories.Interface
{
    public interface IPostRepository:IRepository<Post>
    {

        List<Post> GetByForumId(long forumId);
        List<Post> SearchPosts(string query);

    }
}