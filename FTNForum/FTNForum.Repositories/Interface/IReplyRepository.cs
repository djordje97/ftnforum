using FTNForum.Data.Entity;
using System.Collections.Generic;

namespace FTNForum.Repositories.Interface
{
    public interface IReplyRepository:IRepository<Reply>
    {
        List<Reply> GetByPostId(long postId);
    }
}