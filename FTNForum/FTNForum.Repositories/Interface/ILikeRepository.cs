using FTNForum.Data.Entity;

namespace FTNForum.Repositories.Interface
{
    public interface ILikeRepository:IRepository<Like>
    {
        bool GetLikeByUserAndReply(long userId, long replyId);
    }
}