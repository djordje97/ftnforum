using FTNForum.Data.Entity;

namespace FTNForum.Repositories.Interface
{
    public interface IRoleRepository:IRepository<Role>
    {
        
    }
}