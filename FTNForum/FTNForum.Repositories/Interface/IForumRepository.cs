using FTNForum.Data.Entity;

namespace FTNForum.Repositories.Interface
{
    public interface IForumRepository:IRepository<Forum>
    {
        
    }
}