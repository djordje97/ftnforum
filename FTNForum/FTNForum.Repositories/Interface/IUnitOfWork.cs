using System;

namespace FTNForum.Repositories.Interface
{
    public interface IUnitOfWork : IDisposable
    {
        IForumRepository Forums { get; }
        ILikeRepository Likes { get; }
        IPostRepository Posts { get; }
        IReplyRepository Replies { get; }
        IUserRepository Users { get; }
        IRoleRepository Roles { get; }
        int Complete();
    }
}