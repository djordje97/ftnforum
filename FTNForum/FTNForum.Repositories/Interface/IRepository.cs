using System.Collections.Generic;

namespace FTNForum.Repositories.Interface
{
    public interface IRepository<T> where  T:class
    {
        List<T> GetAll();
        T GetById(long id);
        T Add(T entity);
        T Update(T entity);
        void Delete(T entity);
    }
}