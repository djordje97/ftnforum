using System.Collections.Generic;
using FTNForum.Data.Entity;

namespace FTNForum.Repositories.Interface
{
    public interface IUserRepository:IRepository<User>
    {
       
        User GetByUsername(string username);
        string GetUserPassword(long id);

    }
}