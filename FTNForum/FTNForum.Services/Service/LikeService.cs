using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FTNForum.Data.Entity;
using FTNForum.Repositories.Interface;
using FTNForum.Services.DTO;
using FTNForum.Services.Interface;
using FTNForum.Services.Utils;

namespace FTNForum.Services.Service
{
    public class LikeService : ILikeService
    {
        
        private readonly IUnitOfWork _unitOfWork;
        public LikeService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ServiceResponse<List<LikeDto>> GetAll()
        {
            try
            {
                var entityList = _unitOfWork.Likes.GetAll();
                return ServiceResponse<List<LikeDto>>.CreateResponse(entityList.ToDTOs().ToList(),true);
            }
            catch (Exception e)
            {
                return ServiceResponse<List<LikeDto>>.CreateResponse(null, false,
                    ErrorMessageEnum.ServerError.ToDescription());
            }
        }

        public ServiceResponse<LikeDto> GetOne(long id)
        {
            try
            {
                var entity = _unitOfWork.Likes.GetById(id);
                return ServiceResponse<LikeDto>.CreateResponse(entity.ToDTO(),true);
            }
            catch (Exception e)
            {
                return ServiceResponse<LikeDto>.CreateResponse(null,false,
                ErrorMessageEnum.ServerError.ToDescription());
            }
        }

        public ServiceResponse<LikeDto> Add(Like entity)
        {
            try
            {
                var loggedUser = CommonHelper.GetLoggedUser(_unitOfWork);
                var likeExist = _unitOfWork.Likes.GetLikeByUserAndReply(loggedUser.Id, entity.ReplyId);
                if (likeExist)
                    return null;
                entity.UserId = loggedUser.Id;
                entity.IsLike = true;
                entity.CreatedAt= DateTime.Now;
                entity = _unitOfWork.Likes.Add(entity);
                _unitOfWork.Complete();
                return ServiceResponse<LikeDto>.CreateResponse(entity.ToDTO(),true);
            }
            catch (Exception e)
            {
                return ServiceResponse<LikeDto>.CreateResponse(null,false,
                    ErrorMessageEnum.ServerError.ToDescription());
            }
        }

        public ServiceResponse<LikeDto> Update(Like entity)
        {
            try
            {
                entity.ModifiedAt=DateTime.Now;
                entity = _unitOfWork.Likes.Update(entity);
                _unitOfWork.Complete();
                return ServiceResponse<LikeDto>.CreateResponse(entity.ToDTO(),true);
            }
            catch (Exception e)
            {
                return ServiceResponse<LikeDto>.CreateResponse(null,false,
                    ErrorMessageEnum.ServerError.ToDescription());
            }
        }

        public void Delete(long id)
        {
            var entity = _unitOfWork.Likes.GetById(id);
            _unitOfWork.Likes.Delete(entity);
            _unitOfWork.Complete();
        }
    }
}