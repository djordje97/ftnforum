using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using FTNForum.Data.Entity;
using FTNForum.Repositories.Interface;
using FTNForum.Services.DTO;
using FTNForum.Services.Interface;
using FTNForum.Services.Utils;
using Microsoft.AspNetCore.Mvc;

namespace FTNForum.Services.Service
{
    public class ForumService : IForumService
    {
        private readonly IUnitOfWork _unitOfWork;
        private static readonly IEnumerable<string> RequiredFields = new List<string> { "Title","Description"};

        public ForumService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ServiceResponse<List<ForumDto>>GetAll([FromQuery] PaginationQuery request)
        {
            try
            {
                var entityList = _unitOfWork.Forums.GetAll().ToList();
                var skip = (request.Page - 1) * request.PageSize;
                var total = entityList.Count;
                entityList = entityList.Skip(skip).Take(request.PageSize).ToList();
                return ServiceResponse<List<ForumDto>>.CreateResponse(entityList.ToDTOs().ToList(),true,page:request.Page,pageSize:request.PageSize,totalCount:total);
            }
            catch (Exception e)
            {
                return ServiceResponse<List<ForumDto>>.CreateResponse(null, false,
                    ErrorMessageEnum.ServerError.ToDescription());
            }

        }

        public ServiceResponse<ForumDto> GetOne(long id)
        {
            try
            {
                var entity = _unitOfWork.Forums.GetById(id);
                return ServiceResponse<ForumDto>.CreateResponse(entity.ToDTO(), true);
            }
            catch (Exception e)
            {
                return ServiceResponse<ForumDto>.CreateResponse(null, false,
                    ErrorMessageEnum.ServerError.ToDescription());
            }
        }

        public ServiceResponse<ForumDto> Add(Forum entity)
        {

            try
            {
                var validationResponse = CommonHelper.ValidateRequest(entity,RequiredFields);
                if(!validationResponse.IsPassed)
                    return ServiceResponse<ForumDto>.CreateResponse(null, false,validationResponse.Message);
                entity.CreatedAt = DateTime.Now;
                entity = _unitOfWork.Forums.Add(entity);
                _unitOfWork.Complete();
                return ServiceResponse<ForumDto>.CreateResponse(entity.ToDTO(),true);
            }
            catch (Exception e)
            {
                return ServiceResponse<ForumDto>.CreateResponse(null, false,
                    ErrorMessageEnum.ServerError.ToDescription());
            }
          
        }

        public ServiceResponse<ForumDto> Update(Forum entity)
        {
            try
            {
                var validationResponse = CommonHelper.ValidateRequest(entity,RequiredFields);
                if (!validationResponse.IsPassed)
                    return ServiceResponse<ForumDto>.CreateResponse(null, false,validationResponse.Message);
                entity.ModifiedAt = DateTime.Now;
                entity=_unitOfWork.Forums.Update(entity);
                _unitOfWork.Complete();
                return ServiceResponse<ForumDto>.CreateResponse(entity.ToDTO(),true);

            }
            catch (Exception e)
            {
                return ServiceResponse<ForumDto>.CreateResponse(null, false,
                    ErrorMessageEnum.ServerError.ToDescription());
            }
          
        }

        public void Delete(long id)
        {
            var entity = _unitOfWork.Forums.GetById(id);
            _unitOfWork.Forums.Delete(entity);
            _unitOfWork.Complete();
        }
    }
}