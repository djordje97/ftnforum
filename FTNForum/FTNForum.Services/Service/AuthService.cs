using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using FTNForum.Repositories.Interface;
using FTNForum.Services.DTO;
using FTNForum.Services.Interface;
using FTNForum.Services.Utils;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;



namespace FTNForum.Services.Service
{
    public class AuthService : IAuthService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly IUserService _userService;
        

        public AuthService(IUnitOfWork unitOfWork,IConfiguration configuration,IUserService userService)
        {
            _unitOfWork = unitOfWork;
            _configuration = configuration;
            _userService = userService;
        }

        public ServiceResponse<LoginResultDTO> Login(LoginDto request)
        {
            var hasher = new PasswordHasher<LoginDto>();
            var userFromDb = _unitOfWork.Users.GetByUsername(request.Username);
            if (!userFromDb.IsActive && userFromDb.ModifiedAt != null)
            {
                return ServiceResponse<LoginResultDTO>.CreateResponse(null, false,
                    ErrorMessageEnum.InActiveAccount.ToDescription());
               
            }
            
            if (!userFromDb.IsActive)
            {
                return ServiceResponse<LoginResultDTO>.CreateResponse(null, false,
                    ErrorMessageEnum.InActiveAccountNewUser.ToDescription());
            }
            var isVerify = hasher.VerifyHashedPassword(null, userFromDb.Password, request.Password);
            if (isVerify == PasswordVerificationResult.Success)
            {
                var claims = new List<Claim>();
                var roleClaim = userFromDb.Role.Name == "Admin"
                    ? new Claim(ClaimTypes.Role, "Admin")
                    : new Claim(ClaimTypes.Role, "User");
                
                claims.Add(roleClaim);

                claims.Add(new Claim(ClaimTypes.Name, userFromDb.Id.ToString()));
                var secretKey =
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("JWT:Secret").Value));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256Signature);

                var tokeOptions = new JwtSecurityToken(
                    expires: DateTime.Now.AddHours(5),
                    signingCredentials: signinCredentials,
                    claims: claims
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
                var successLogin = new LoginResultDTO
                {
                    Token = tokenString,
                    LoggedUser = EntityConverter.ToDTO(userFromDb),
                    IsSuccess =  true
                };
                return ServiceResponse<LoginResultDTO>.CreateResponse(successLogin, true,
                    ErrorMessageEnum.InActiveAccount.ToDescription());
            }

            return ServiceResponse<LoginResultDTO>.CreateResponse(null, false,
                ErrorMessageEnum.IncorrectPasswordOrUsername.ToDescription());
        }

        public ServiceResponse<UserDto> ChangePassword(ChangePasswordDto passwordDto)
        {
            try
            {
                var userEntity = passwordDto.User.ToEntity();
                var currentPassword = _userService.GetUserPassword(userEntity.Id);
                var hasher = new PasswordHasher<LoginDto>();
                var isVerify = hasher.VerifyHashedPassword(null, currentPassword, passwordDto.CurrentPassword);
                if (isVerify == PasswordVerificationResult.Failed)
                    return ServiceResponse<UserDto>.CreateResponse(null, false, ErrorMessageEnum.ChangePasswordError.ToDescription());
                userEntity.Password = hasher.HashPassword(null, passwordDto.NewPassword);
                userEntity = _unitOfWork.Users.Update(userEntity);
                _unitOfWork.Complete();
                return ServiceResponse<UserDto>.CreateResponse(userEntity.ToDTO(), true);
            }
            catch (Exception e)
            {
                return ServiceResponse<UserDto>.CreateResponse(null, false,ErrorMessageEnum.ServerError.ToDescription());

            }
        }
    }
}