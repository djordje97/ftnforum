using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using AutoMapper;
using FTNForum.Data.Entity;
using FTNForum.Repositories.Interface;
using FTNForum.Services.DTO;
using FTNForum.Services.Interface;
using FTNForum.Services.Utils;
using Microsoft.AspNetCore.Http;

namespace FTNForum.Services.Service
{
    public class ReplyService : IReplyService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private  static  readonly  IEnumerable<string> RequiredFields = new List<string>{"PostId","Content"};
        public ReplyService(IUnitOfWork unitOfWork,IHttpContextAccessor httpContextAccessor)
        {
            _unitOfWork = unitOfWork;
            _httpContextAccessor = httpContextAccessor;
        }

        public ServiceResponse<List<ReplyDto>> GetAll()
        {
            try
            {
                var entityList = _unitOfWork.Replies.GetAll();
                return ServiceResponse<List<ReplyDto>>.CreateResponse(entityList.ToDTOs().ToList(),
                    true);
            }
            catch (Exception e)
            {
                return ServiceResponse<List<ReplyDto>>.CreateResponse(null, false,
                    ErrorMessageEnum.ServerError.ToDescription());
            }
        }

        public ServiceResponse<ReplyDto> GetOne(long id)
        {
            try
            {
                var entity = _unitOfWork.Replies.GetById(id);
                return ServiceResponse<ReplyDto>.CreateResponse(entity.ToDTO(),
                    true);
            }
            catch (Exception e)
            {
                return ServiceResponse<ReplyDto>.CreateResponse(null, false,
                    ErrorMessageEnum.ServerError.ToDescription());
            }
        }

        public ServiceResponse<ReplyDto> Add(Reply entity)
        {
            try
            {
                var loggedUser = CommonHelper.GetLoggedUser(_unitOfWork);
                var validatorResponse = CommonHelper.ValidateRequest(entity, RequiredFields);
                if(!validatorResponse.IsPassed)
                    return ServiceResponse<ReplyDto>.CreateResponse(null,false,validatorResponse.Message);
                var post = _unitOfWork.Posts.GetById(entity.PostId);
                entity.User = loggedUser;
                entity.Post = post;
                entity.CreatedAt = DateTime.Now;
                entity = _unitOfWork.Replies.Add(entity);
                _unitOfWork.Complete();
                return ServiceResponse<ReplyDto>.CreateResponse(entity.ToDTO(),
                    true);
            }
            catch (Exception e)
            {
                return ServiceResponse<ReplyDto>.CreateResponse(null, false,
                    ErrorMessageEnum.ServerError.ToDescription());
            }
        }

        public ServiceResponse<ReplyDto> Update(Reply entity)
        {

            try
            {
                var validatorResponse = CommonHelper.ValidateRequest(entity, RequiredFields);
                if(!validatorResponse.IsPassed)
                    return ServiceResponse<ReplyDto>.CreateResponse(null,false,validatorResponse.Message);
                entity.ModifiedAt = DateTime.Now;
                entity = _unitOfWork.Replies.Update(entity);
                _unitOfWork.Complete();
                return ServiceResponse<ReplyDto>.CreateResponse(entity.ToDTO(),
                    true);
            }
            catch (Exception e)
            {
                
                return ServiceResponse<ReplyDto>.CreateResponse(null, false,
                    ErrorMessageEnum.ServerError.ToDescription());
            }
        }

        public void Delete(long id)
        {
            var entity = _unitOfWork.Replies.GetById(id);
            _unitOfWork.Replies.Delete(entity);
            _unitOfWork.Complete();
        }

        public ServiceResponse<List<ReplyDto>> GetByPostId(long id)
        {
            try
            {
                var entityList = _unitOfWork.Replies.GetByPostId(id);
            
                return ServiceResponse<List<ReplyDto>>.CreateResponse(entityList.ToDTOs().ToList(),
                    true);
            }
            catch (Exception e)
            {
               
                return ServiceResponse<List<ReplyDto>>.CreateResponse(null, false,
                    ErrorMessageEnum.ServerError.ToDescription());
            }
        }
    }
}