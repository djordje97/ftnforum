using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FTNForum.Data.Entity;
using FTNForum.Repositories.Interface;
using FTNForum.Services.DTO;
using FTNForum.Services.Interface;
using FTNForum.Services.Utils;

namespace FTNForum.Services.Service
{
    public class UserService : IUserService

    {
        private readonly IUnitOfWork _unitOfWork;
        private static readonly IEnumerable<string> RequiredFields = new List<string> { "Username","Password","Email","Name"};

        
        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ServiceResponse<List<UserDto>> GetAll(PaginationQuery request)
        {
            try
            {
                var entityList = _unitOfWork.Users.GetAll().ToList();
                var skip = (request.Page - 1) * request.PageSize;
                var total = entityList.Count;
                entityList = entityList.Skip(skip).Take(request.PageSize).ToList();

                return ServiceResponse<List<UserDto>>.CreateResponse(entityList.ToDTOs().ToList(),true,page:request.Page,pageSize:request.PageSize,totalCount:total);
            }
            catch (Exception e)
            {
                return ServiceResponse<List<UserDto>>.CreateResponse(null,false,ErrorMessageEnum.ServerError.ToDescription());

            }
            
        }

        public ServiceResponse<UserDto> GetOne(long id)
        {
            try
            {
                var entity = _unitOfWork.Users.GetById(id);
                return ServiceResponse<UserDto>.CreateResponse(entity.ToDTO(),true);
            }
            catch (Exception e)
            {
                return ServiceResponse<UserDto>.CreateResponse(null,false,ErrorMessageEnum.ServerError.ToDescription());
            }
        }

        public ServiceResponse<UserDto> GetByUsername(string username)
        {
            try
            {
                var entity = _unitOfWork.Users.GetByUsername(username);
                return  ServiceResponse<UserDto>.CreateResponse(entity.ToDTO(),true);
 
            }
            catch (Exception e)
            {
                return ServiceResponse<UserDto>.CreateResponse(null,false,ErrorMessageEnum.ServerError.ToDescription());

            }
        }

        public ServiceResponse<UserDto> Add(User entity)
        {
            try
            {
                var validationResponse = CommonHelper.ValidateRequest(entity, RequiredFields);
                if (!validationResponse.IsPassed)
                    return ServiceResponse<UserDto>.CreateResponse(null, false, validationResponse.Message);
                var role = _unitOfWork.Roles.GetById(1);
                entity.RoleId = role.Id;
                entity.IsActive = false;
                entity.CreatedAt = DateTime.Now;
                entity = _unitOfWork.Users.Add(entity);
                _unitOfWork.Complete();
                return ServiceResponse<UserDto>.CreateResponse(entity.ToDTO(),true);
            }
            catch (Exception e)
            {
                return ServiceResponse<UserDto>.CreateResponse(null,false,ErrorMessageEnum.ServerError.ToDescription());

            }
        }
        

        public ServiceResponse<UserDto> Update(User entity)
        {

            try
            {
                entity.Password = GetUserPassword(entity.Id);
                var validationResponse = CommonHelper.ValidateRequest(entity, RequiredFields);
                if (!validationResponse.IsPassed)
                    return ServiceResponse<UserDto>.CreateResponse(null, false, validationResponse.Message);
                entity.ModifiedAt = DateTime.Now;
                _unitOfWork.Users.Update(entity);
                _unitOfWork.Complete();
                return ServiceResponse<UserDto>.CreateResponse(entity.ToDTO(),true);
            }
            catch (Exception e)
            {
                return ServiceResponse<UserDto>.CreateResponse(null,false,ErrorMessageEnum.ServerError.ToDescription());

            }
        }

        public void Delete(long id)
        {
            var entity = _unitOfWork.Users.GetById(id);
            _unitOfWork.Users.Delete(entity);
            _unitOfWork.Complete();
        }

        public string GetUserPassword(long id)
        {
            var password = _unitOfWork.Users.GetUserPassword(id);
            return password;

        }

        public ServiceResponse<UserDto> PromoteUserToAdmin(User entity)
        {
            try
            {
                entity.Password = GetUserPassword(entity.Id);
                var role = _unitOfWork.Roles.GetById(2);
                entity.Role = role;
                _unitOfWork.Users.Update(entity);
                _unitOfWork.Complete();
                return ServiceResponse<UserDto>.CreateResponse(entity.ToDTO(), true);
            }
            catch (Exception)
            {
                return ServiceResponse<UserDto>.CreateResponse(null,false,ErrorMessageEnum.ServerError.ToDescription());

            }
        }
    }
}