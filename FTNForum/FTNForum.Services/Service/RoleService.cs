using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FTNForum.Data.Entity;
using FTNForum.Repositories.Interface;
using FTNForum.Services.DTO;
using FTNForum.Services.Interface;

namespace FTNForum.Services.Service
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork _unitOfWork;

        public RoleService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<Role> GetAll()
        {
            var entityList = _unitOfWork.Roles.GetAll();
            return entityList;
        }

        public Role GetOne(long id)
        {
            var entity = _unitOfWork.Roles.GetById(id);
            return entity;
        }

        public Role Add(Role entity)
        {
            entity = _unitOfWork.Roles.Add(entity);
            _unitOfWork.Complete();
            return entity;
        }

        public Role Update(Role entity)
        {
            
            entity = _unitOfWork.Roles.Update(entity);
            _unitOfWork.Complete();
            return entity;
        }

        public void Delete(long id)
        {
            var entity = _unitOfWork.Roles.GetById(id);
            _unitOfWork.Roles.Delete(entity);
            _unitOfWork.Complete();
        }
    }
}