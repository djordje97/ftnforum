using System;
using System.IO;
using FTNForum.Data.Entity;
using FTNForum.Services.Interface;
using FTNForum.Services.Utils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace FTNForum.Services.Service
{
    public class UtilService: IUtilService
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IForumService _forumService;

        public UtilService(IHostingEnvironment hostingEnvironment,IForumService forumService)
        {
            _hostingEnvironment = hostingEnvironment;
            _forumService = forumService;
        }
        public ServiceResponse<string> UploadFile(IFormCollection formCollection)
        {
            try
            {
                var file = formCollection.Files[0];
                var guid = Guid.NewGuid();
                var newFileName = String.Format("{0}{1}", guid, file.FileName);
                var uploadDir = Path.Combine(_hostingEnvironment.ContentRootPath, "FileStorage");
                var filePath = Path.Combine(uploadDir, newFileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(stream);
                    stream.Close();
                }

                

                return ServiceResponse<string>.CreateResponse(filePath,true);
            }
            catch (Exception )
            {
                return ServiceResponse<string>.CreateResponse(null,false,ErrorMessageEnum.ServerError.ToDescription());
            }
        }
    }
}