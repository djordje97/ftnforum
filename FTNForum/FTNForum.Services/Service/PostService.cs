using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FTNForum.Data.Entity;
using FTNForum.Repositories.Interface;
using FTNForum.Services.DTO;
using FTNForum.Services.Interface;
using FTNForum.Services.Utils;
using Microsoft.AspNetCore.Http;

namespace FTNForum.Services.Service
{
    public class PostService : IPostService
    {
        private static readonly IEnumerable<string> RequiredField = new List<string>{"ForumId","Title","Content"};
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public PostService(IUnitOfWork unitOfWork,IHttpContextAccessor httpContextAccessor)
        {
            _unitOfWork = unitOfWork; ;
            _httpContextAccessor = httpContextAccessor;
        }

        public ServiceResponse<List<PostDto>> GetAll()
        {
            try
            {
                var entityList = _unitOfWork.Posts.GetAll();
                return ServiceResponse<List<PostDto>>.CreateResponse(entityList.ToDTOs().ToList(),true);
            }
            catch (Exception e)
            {
                return ServiceResponse<List<PostDto>>.CreateResponse(null, false, e.Message);
            }
        }
        
        public ServiceResponse<List<PostDto>> SearchPosts(PaginationQuery request)
        {
            try
            {
                var skip = (request.Page - 1) * request.PageSize;
                var entityList = _unitOfWork.Posts.SearchPosts(request.Data.ToString());
                var total = entityList.Count;
                entityList = entityList.Skip(skip).Take(request.PageSize).ToList();
                return ServiceResponse<List<PostDto>>.CreateResponse(entityList.ToDTOs().ToList(),true,page:request.Page,pageSize:request.PageSize,totalCount:total);
            }
            catch (Exception e)
            {
                return ServiceResponse<List<PostDto>>.CreateResponse(null, false, e.Message);
            }
        }

        public ServiceResponse<List<PostDto>> GetLatest()
        {
            try
            {
                var entityList = _unitOfWork.Posts.GetAll();
                entityList = entityList.OrderByDescending(x => x.CreatedAt).Take(5).ToList();
                return ServiceResponse<List<PostDto>>.CreateResponse(entityList.ToDTOs().ToList(),true);
            }
            catch (Exception e)
            {
                return ServiceResponse<List<PostDto>>.CreateResponse(null, false, e.Message);

            }
        }

        public ServiceResponse<List<PostDto>> GetByForumId(PaginationQuery request)
        {
            try
            {
                var forumId = Int32.Parse(request.Data);
                var skip = (request.Page - 1) * request.PageSize;
                var entityList = _unitOfWork.Posts.GetByForumId(forumId);
                var total = entityList.Count;
                entityList = entityList.Skip(skip).Take(request.PageSize).ToList();
                return ServiceResponse<List<PostDto>>.CreateResponse(entityList.ToDTOs().ToList(),true,page:request.Page,pageSize:request.PageSize,totalCount:total);
            }
            catch (Exception e)
            {
                return ServiceResponse<List<PostDto>>.CreateResponse(null, false, ErrorMessageEnum.ServerError.ToDescription());

            }
        }

        public ServiceResponse<PostDto> GetOne(long id)
        {
            try
            {
                var entity = _unitOfWork.Posts.GetById(id);
                return ServiceResponse<PostDto>.CreateResponse(entity.ToDTO(),true);
            }
            catch (Exception e)
            {
                return ServiceResponse<PostDto>.CreateResponse(null, false, ErrorMessageEnum.ServerError.ToDescription());

            }
        }

        public ServiceResponse<PostDto> Add(Post entity)
        {
            try
            {
                var validationResponse = CommonHelper.ValidateRequest(entity,RequiredField );
                if (!validationResponse.IsPassed)
                    return ServiceResponse<PostDto>.CreateResponse(null, false, validationResponse.Message);
                var loggedUser = CommonHelper.GetLoggedUser(_unitOfWork);
                var forum = _unitOfWork.Forums.GetById(entity.ForumId);
                entity.CreatedAt = DateTime.Now;
                entity.Forum = forum;
                entity.User = loggedUser;
                entity = _unitOfWork.Posts.Add(entity);
                _unitOfWork.Complete();
                return ServiceResponse<PostDto>.CreateResponse(entity.ToDTO(),true);
            }
            catch (Exception e)
            {
                return ServiceResponse<PostDto>.CreateResponse(null, false,ErrorMessageEnum.ServerError.ToDescription());

            }
        }

        public ServiceResponse<PostDto> Update(Post entity)
        {

            try
            {
                var validationResponse = CommonHelper.ValidateRequest(entity,RequiredField );
                if(!validationResponse.IsPassed)
                    return ServiceResponse<PostDto>.CreateResponse(null, false, validationResponse.Message);
                entity.ModifiedAt = DateTime.Now;
                entity = _unitOfWork.Posts.Update(entity);
                _unitOfWork.Complete();
                return ServiceResponse<PostDto>.CreateResponse(entity.ToDTO(),true);
            }
            catch (Exception e)
            {
                return ServiceResponse<PostDto>.CreateResponse(null, false,ErrorMessageEnum.ServerError.ToDescription());

            }
        }

        public void Delete(long id)
        {
            var entity = _unitOfWork.Posts.GetById(id);
            _unitOfWork.Posts.Delete(entity);
            _unitOfWork.Complete();
        }
    }
}