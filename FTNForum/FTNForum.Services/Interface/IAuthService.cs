using FTNForum.Services.DTO;
using FTNForum.Services.Utils;

namespace FTNForum.Services.Interface
{
    public interface IAuthService
    {
        ServiceResponse<LoginResultDTO> Login(LoginDto request);
        ServiceResponse<UserDto> ChangePassword(ChangePasswordDto passwordDto);
    }
}