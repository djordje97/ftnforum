using System.Collections.Generic;
using FTNForum.Data.Entity;
using FTNForum.Services.DTO;
using FTNForum.Services.Utils;

namespace FTNForum.Services.Interface
{
    public interface ILikeService
    {
        public ServiceResponse<List<LikeDto>> GetAll();
        public ServiceResponse<LikeDto> GetOne(long id);
        public ServiceResponse<LikeDto> Add(Like dto);
        public ServiceResponse<LikeDto> Update(Like dto);
        public void Delete(long id);
    }
}