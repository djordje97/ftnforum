using System.Collections.Generic;
using FTNForum.Data.Entity;
using FTNForum.Services.DTO;

namespace FTNForum.Services.Interface
{
    public interface IRoleService
    {
        public List<Role> GetAll();
        public Role GetOne(long id);
        public Role Add(Role dto);
        public Role Update(Role dto);
        public void Delete(long id);
    }
}