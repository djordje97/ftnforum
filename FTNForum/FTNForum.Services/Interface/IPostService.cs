using System.Collections.Generic;
using FTNForum.Data.Entity;
using FTNForum.Services.DTO;
using FTNForum.Services.Utils;

namespace FTNForum.Services.Interface
{
    public interface IPostService
    {
        public ServiceResponse<List<PostDto>> GetAll();
        public ServiceResponse<List<PostDto>> SearchPosts( PaginationQuery request);
        public ServiceResponse< List<PostDto>> GetLatest();
        public ServiceResponse<List<PostDto>> GetByForumId(PaginationQuery request);
        public ServiceResponse<PostDto> GetOne(long id);
        public ServiceResponse<PostDto> Add(Post dto);
        public ServiceResponse<PostDto> Update(Post dto);
        public void Delete(long id);
        
    }
}