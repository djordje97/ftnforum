using System.Collections.Generic;
using FTNForum.Data.Entity;
using FTNForum.Services.DTO;
using FTNForum.Services.Utils;

namespace FTNForum.Services.Interface
{
    public interface IUserService
    {
        public ServiceResponse<List<UserDto>> GetAll(PaginationQuery request);
        public ServiceResponse<UserDto> GetOne(long id);
        public ServiceResponse<UserDto> GetByUsername(string username);
        public ServiceResponse<UserDto> Add(User dto);
        public ServiceResponse<UserDto> Update(User entity);
        public void Delete(long id);

        public string GetUserPassword(long id);

        public ServiceResponse<UserDto> PromoteUserToAdmin(User entity);
    }
}