using FTNForum.Data.Entity;
using FTNForum.Services.Utils;
using Microsoft.AspNetCore.Http;

namespace FTNForum.Services.Interface
{
    public interface IUtilService
    {
        public ServiceResponse<string> UploadFile(IFormCollection formCollection);
    }
}