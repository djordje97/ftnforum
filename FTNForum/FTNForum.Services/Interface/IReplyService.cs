using System.Collections.Generic;
using FTNForum.Data.Entity;
using FTNForum.Services.DTO;
using FTNForum.Services.Utils;

namespace FTNForum.Services.Interface
{
    public interface IReplyService
    {
        public ServiceResponse<List<ReplyDto>> GetAll();
        public ServiceResponse<List<ReplyDto>> GetByPostId(long id);
        public ServiceResponse<ReplyDto> GetOne(long id);
        public ServiceResponse<ReplyDto> Add(Reply dto);
        public ServiceResponse<ReplyDto> Update(Reply dto);
        public void Delete(long id);
    }
}