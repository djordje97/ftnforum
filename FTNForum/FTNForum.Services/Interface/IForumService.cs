using System.Collections.Generic;
using FTNForum.Data.Entity;
using FTNForum.Services.DTO;
using FTNForum.Services.Utils;
using Microsoft.AspNetCore.Mvc;

namespace FTNForum.Services.Interface
{
    public interface IForumService
    {
        public ServiceResponse<List<ForumDto>> GetAll([FromQuery] PaginationQuery request);
        public ServiceResponse<ForumDto> GetOne(long id);
        public ServiceResponse<ForumDto> Add(Forum dto);
        public ServiceResponse<ForumDto> Update(Forum dto);
        public void Delete(long id);
    }
}