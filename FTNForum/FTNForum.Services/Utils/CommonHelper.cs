using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using FTNForum.Data.Entity;
using FTNForum.Repositories;
using FTNForum.Repositories.Interface;
using FTNForum.Repositories.Repository;
using FTNForum.Services.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace FTNForum.Services.Utils
{
    public class CommonHelper
    {
        public static ValidatorResponse ValidateRequest<T>(T entity,IEnumerable<string> fieldsForValidation = null)
        {
            var validationResponse = new ValidatorResponse
            {
                IsPassed = true,
                Message = ""
            };
            fieldsForValidation ??= new List<string>();
            IEnumerable<PropertyInfo> properties = entity.GetType().GetProperties()
                .Where(x => fieldsForValidation.Contains(x.Name)).ToList();
            foreach (var property in properties)
            {
                if (property.GetValue(entity,null) == null || string.IsNullOrEmpty(property.GetValue(entity,null)?.ToString()) )
                {
                    validationResponse.IsPassed = false;
                    validationResponse.Message += $"{property.Name} is required,";
                }
                
            }

            return validationResponse;
        }
        
        public static string GetDescription(object enumVal, Type tp = null)
        {
            if (tp == null) tp = enumVal.GetType();

            FieldInfo field = tp.GetField(enumVal.ToString());

            DescriptionAttribute attribute
                = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute))
                    as DescriptionAttribute;

            return attribute == null ? enumVal.ToString() : attribute.Description;

        }

        public static User GetLoggedUser(IUnitOfWork unitOfWork)
        {
            var context = new HttpContextAccessor();
            var idString = context.HttpContext.User.Identity.Name;
            var loggedUserId =Int32.Parse( idString!);
            var loggedUser = unitOfWork.Users.GetById(loggedUserId);
            return loggedUser;
        }
        public static T GetFromQueryString<T>() where T : new(){
            var obj = new T();
            var properties = typeof(T).GetProperties();
            var context = new HttpContextAccessor();
            var querySting = context.HttpContext.Request.Query;
            foreach(var property in properties){
                if (property != null)
                {
                    var valueAsString = querySting.FirstOrDefault(x => x.Key == property.Name).Value; 
                    var value = Parse(property.PropertyType,valueAsString);

                    if(value == null)
                        continue;

                    property.SetValue(obj, value, null);
                }
            }
            return obj;
        }
        private static object Parse(Type dataType, string ValueToConvert)  
        {  
            TypeConverter obj = TypeDescriptor.GetConverter(dataType);
            if (string.IsNullOrEmpty(ValueToConvert))
                return null;
            object value = obj.ConvertFromString(null, CultureInfo.InvariantCulture, ValueToConvert);  
            return value;  
        }   
    }

    public class ValidatorResponse
    {
        public bool IsPassed { get; set; }
        public string Message { get; set; }
    }
    
    
    
}