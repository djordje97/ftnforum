using Microsoft.AspNetCore.Mvc;

namespace FTNForum.Services.Utils
{
    public class PaginationQuery
    {
        public string Data { get; set; }
        [FromQuery(Name = "TotalCount")]

        public int TotalCount { get; set; }
        [FromQuery(Name = "PageSize")]

        public int PageSize { get; set; } = 3;
        [FromQuery(Name = "Page")]

        public int Page { get; set; } = 1;
    }
}