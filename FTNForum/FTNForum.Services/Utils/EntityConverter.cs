using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FTNForum.Data.Entity;
using FTNForum.Services.DTO;

namespace FTNForum.Services.Utils
{
    public static  class EntityConverter
    {
        #region[Forum]
        public static ForumDto ToDTO(this Forum entity)
        {
            return new ForumDto()
            {
               Id =  entity.Id,
               Title = entity.Title,
               Description = entity.Description,
               CreatedAt = entity.CreatedAt,
               Image =  GetImageStream(entity.ImageUrl),
               ImageUrl = entity.ImageUrl
            };
        }

        public static Forum ToEntity(this ForumDto dto)
        {
            return new Forum()
            {
                Id = dto.Id,
                Title = dto.Title,
                Description = dto.Description,
                ImageUrl = dto.ImageUrl,
                CreatedAt = dto.CreatedAt
            };
        }

        public static IEnumerable<ForumDto> ToDTOs(this IEnumerable<Forum> entities)
        {
            return entities.Select(e => e.ToDTO());
        }

        public static IEnumerable<Forum> ToEntities(this IEnumerable<ForumDto> dtos)
        {
            return dtos.Select(d => d.ToEntity());
        }
        #endregion
        
        #region[Post]
        public static PostDto ToDTO(this Post entity)
        {
            return new PostDto()
            {
                Id =  entity.Id,
                Title = entity.Title,
                Content = entity.Content,
                CreatedAt = entity.CreatedAt,
                UserId = entity.UserId,
                ForumId = entity.ForumId,
                Forum = entity.Forum!= null?ToDTO(entity.Forum):null,
                User =  entity.User!= null?ToDTO(entity.User):null,
                RepliesCount =  entity.Replies.Count
            };
        }

        public static Post ToEntity(this PostDto dto)
        {
            return new Post()
            {
                Id = dto.Id,
                Title = dto.Title,
                Content = dto.Content,
                ForumId = dto.ForumId,
                UserId = dto.UserId,
                Forum = dto.Forum!=null?ToEntity(dto.Forum):null,
                User = dto.User!=null?ToEntity(dto.User):null,
                CreatedAt = dto.CreatedAt
            };
        }

        public static IEnumerable<PostDto> ToDTOs(this IEnumerable<Post> entities)
        {
            return entities.Select(e => e.ToDTO());
        }

        public static IEnumerable<Post> ToEntities(this IEnumerable<PostDto> dtos)
        {
            return dtos.Select(d => d.ToEntity());
        }
        #endregion
        
        #region[User]
        public static UserDto ToDTO(this User entity)
        {
            return new UserDto()
            {
                Id =  entity.Id,
                Name = entity.FirstName,
                Surname = entity.Surname,
                Email = entity.Email,
                Password = string.Empty,
                Username = entity.Username,
                IsActive = entity.IsActive,
                RoleId = entity.RoleId,
                CreatedAt = entity.CreatedAt,
                Role = entity.Role != null?ToDTO(entity.Role):null,
                ImageUrl =  entity.ImageUrl,
                Image =  GetImageStream(entity.ImageUrl),

            };
        }

        public static User ToEntity(this UserDto dto)
        {
            return new User()
            {
                Id = dto.Id,
                FirstName = dto.Name,
                Surname = dto.Surname,
                Email = dto.Email,
                Password = dto.Password,
                Username = dto.Username,
                IsActive = dto.IsActive,
                RoleId = dto.RoleId,
                Role = dto.Role!= null?ToEntity(dto.Role):null,
                CreatedAt = dto.CreatedAt,
                ImageUrl = dto.ImageUrl
            };
        }

        public static IEnumerable<UserDto> ToDTOs(this IEnumerable<User> entities)
        {
            return entities.Select(e => e.ToDTO());
        }

        public static IEnumerable<User> ToEntities(this IEnumerable<UserDto> dtos)
        {
            return dtos.Select(d => d.ToEntity());
        }
        #endregion
        
        #region[Role]
        public static RoleDto ToDTO(this Role entity)
        {
            return new RoleDto()
            {
                Id =  entity.Id,
                Name = entity.Name
            };
        }

        public static Role ToEntity(this RoleDto dto)
        {
            return new Role()
            {
                Id = dto.Id,
                Name = dto.Name
            };
        }

        public static IEnumerable<RoleDto> ToDTOs(this IEnumerable<Role> entities)
        {
            return entities.Select(e => e.ToDTO());
        }

        public static IEnumerable<Role> ToEntities(this IEnumerable<RoleDto> dtos)
        {
            return dtos.Select(d => d.ToEntity());
        }
        #endregion
        
        #region[Reply]
        public static ReplyDto ToDTO(this Reply entity)
        {
            return new ReplyDto()
            {
                Id =  entity.Id, 
                Content = entity.Content,
                PostId = entity.PostId,
                UserId = entity.UserId,
                NumberOfLikes =  entity.Likes.Count,
                Post = entity.Post != null?ToDTO(entity.Post):null,
                User =entity.User!= null? ToDTO(entity.User):null,
                CreatedAt = entity.CreatedAt
            };
        }

        public static Reply ToEntity(this ReplyDto dto)
        {
            return new Reply()
            {
                Id = dto.Id,
                Content = dto.Content,
                PostId = dto.PostId,
                UserId = dto.UserId,
                Post = dto.Post!=null?ToEntity(dto.Post):null,
                User = dto.User!= null?ToEntity(dto.User):null
            };
        }

        public static IEnumerable<ReplyDto> ToDTOs(this IEnumerable<Reply> entities)
        {
            return entities.Select(e => e.ToDTO());
        }

        public static IEnumerable<Reply> ToEntities(this IEnumerable<ReplyDto> dtos)
        {
            return dtos.Select(d => d.ToEntity());
        }
        #endregion
        
        #region[Like]
        public static LikeDto ToDTO(this Like entity)
        {
            return new LikeDto()
            {
                Id =  entity.Id, 
                IsLike = entity.IsLike,
                UserId = entity.UserId,
                ReplyId = entity.ReplyId,
                User = entity.User!= null?ToDTO(entity.User):null,
                Reply = entity.Reply!=null?ToDTO(entity.Reply):null,
                CreatedAt = entity.CreatedAt
            };
        }

        public static Like ToEntity(this LikeDto dto)
        {
            return new Like()
            {
                Id = dto.Id,
                IsLike = dto.IsLike,
                ReplyId = dto.ReplyId,
                UserId = dto.UserId,
                Reply = dto.Reply!=null?ToEntity(dto.Reply):null,
                User = dto.User!=null?ToEntity(dto.User):null
            };
        }

        public static IEnumerable<LikeDto> ToDTOs(this IEnumerable<Like> entities)
        {
            return entities.Select(e => e.ToDTO());
        }

        public static IEnumerable<Like> ToEntities(this IEnumerable<LikeDto> dtos)
        {
            return dtos.Select(d => d.ToEntity());
        }
        #endregion

        private static byte[] GetImageStream(string filePath)
        {
            try
            {
                if (String.IsNullOrEmpty(filePath))
                    return null;    
                byte[] fileBytes = File.ReadAllBytes(filePath);
                return fileBytes;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}