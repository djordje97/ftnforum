using System.Collections.Generic;

namespace FTNForum.Services.Utils
{
    public class ServiceResponse<T>
    {

        public T Data { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public int PageSize { get; set; } 
        public int Page { get; set; }
        public int TotalCount { get; set; }
        
        public static ServiceResponse<T> CreateResponse(T entity, bool success,string message ="",int page = 1,int pageSize = 5, int totalCount = 0)
        {
            var newResponse = new ServiceResponse<T>
            {
                Data = entity,
                IsSuccess =  success,
                Message = message,
                PageSize =  pageSize,
                Page = page,
                TotalCount = totalCount
            };
            return newResponse;
        }
    }
}