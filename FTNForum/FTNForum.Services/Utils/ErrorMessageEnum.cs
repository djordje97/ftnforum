using System.ComponentModel;

namespace FTNForum.Services.Utils
{
    public enum ErrorMessageEnum
    {
        [Description("Server error, try again in few minutes")]
        ServerError = 0,
        [Description("Value for current password is incorrect")]
        ChangePasswordError = 1,
        [Description("Your account has to be verified by our admin.Please try again later")]
        InActiveAccountNewUser = 2,
        [Description("Your account has been deactivated. Please contact our admin to fix this")]
        InActiveAccount = 3,
        [Description("Incorrect username or password")]
        IncorrectPasswordOrUsername = 4
    }

    public static class ErrorMessageEnumExtension
    {
        public static ErrorMessageEnum ToEnum(this int val)
        {
            return ToEnum((int?)val);
        }

        public static ErrorMessageEnum ToEnum(this int? val)
        {
            var retval = ErrorMessageEnum.ServerError;

            if (val != null)
            {
                switch (val)
                {
                    case 0:
                        retval = ErrorMessageEnum.ServerError;
                        break;
                    case 1:
                        retval = ErrorMessageEnum.ChangePasswordError;
                        break;
                    case 2:
                        retval = ErrorMessageEnum.InActiveAccountNewUser;
                        break;
                    case 3:
                        retval = ErrorMessageEnum.InActiveAccount;
                        break;
                    case 4:
                        retval = ErrorMessageEnum.IncorrectPasswordOrUsername;
                        break;
                }
            }

            return retval;
        }

        public static int ToInt(this ErrorMessageEnum rl)
        {
            return (int)rl;
        }
        public static string ToDescription(this ErrorMessageEnum rl)
        {
            return CommonHelper.GetDescription(rl);
        }

    }
}