using System;

namespace FTNForum.Services.DTO
{
    public class ForumDto
    {
        public long Id { get; set; }
        public string Title{ get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public byte [] Image { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
    }
}