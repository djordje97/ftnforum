using System;
using FTNForum.Data.Entity;

namespace FTNForum.Services.DTO
{
    public class PostDto
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        public long UserId { get; set; }
        public long ForumId { get; set; }
        public ForumDto Forum{ get; set; }
        public UserDto User { get; set; }
        public int RepliesCount { get; set; }
    }
    
}