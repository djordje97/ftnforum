using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Permissions;

namespace FTNForum.Services.DTO
{
    public class UserDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public string ImageUrl { get; set; }
        public byte[] Image { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        public long RoleId { get; set; }
        public RoleDto Role { get; set; }
    }
}