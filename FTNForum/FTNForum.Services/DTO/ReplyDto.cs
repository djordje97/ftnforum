using System;

namespace FTNForum.Services.DTO
{
    public class ReplyDto
    {
        public long Id { get; set; }
        public string Content { get; set; }
        public int NumberOfLikes { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        public long PostId { get; set; }
        public long UserId { get; set; }
        public  PostDto Post { get; set; }
        public  UserDto User{ get; set; }
    }
}