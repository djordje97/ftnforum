namespace FTNForum.Services.DTO
{
    public class LoginResultDTO
    {
        public string Token { get; set; }
        public UserDto LoggedUser { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}