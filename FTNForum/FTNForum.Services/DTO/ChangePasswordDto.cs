namespace FTNForum.Services.DTO
{
    public class ChangePasswordDto
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public UserDto User { get; set; }
        
    }
}