using System;

namespace FTNForum.Services.DTO
{
    public class LikeDto
    {
        public long Id { get; set; }
        public bool IsLike { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }

        public long ReplyId { get; set; }
        public long UserId { get; set; }

        public  ReplyDto Reply { get; set; }
        public  UserDto User { get; set; }
    }
}