namespace FTNForum.Services.DTO
{
    public class RoleDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}