using FTNForum.Services.DTO;
using FTNForum.Services.Interface;
using FTNForum.Services.Utils;
using Microsoft.AspNetCore.Mvc;

namespace FTNForum.Web.Controllers
{
    [ApiController]
    [Route("api/likes")]
    public class LikeController : ControllerBase
    {
        private readonly ILikeService _likeService;

        public LikeController(ILikeService likeService)
        {
            _likeService = likeService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var entityList = _likeService.GetAll();
            return Ok(entityList);
        }

        [HttpGet("{id}")]
        public IActionResult GetOne(long id)
        {
            var entity = _likeService.GetOne(id);
            if (entity.Data == null)
                return NotFound();
            return Ok(entity);
        }

        [HttpPost]
        public IActionResult Add([FromBody] LikeDto dto)
        {
            if (dto == null)
                return BadRequest();
            var entity = _likeService.Add(dto.ToEntity());
            return Created("localhost:8080/api/likes/" + entity.Data?.Id, entity);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] LikeDto dto)
        {
            if (_likeService.GetOne(id) == null)
                return BadRequest();
            var entity = _likeService.Update(dto.ToEntity());
            return Ok(entity);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var forum = _likeService.GetOne(id);
            if (forum == null)
                return BadRequest();
            _likeService.Delete(id);
            return Ok();
        }
    }
}