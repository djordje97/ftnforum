using FTNForum.Services.DTO;
using FTNForum.Services.Interface;
using FTNForum.Services.Utils;
using FTNForum.Web.Hubs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace FTNForum.Web.Controllers
{
    [ApiController]
    [Route("api/replies")]
    public class ReplyController : ControllerBase
    {
        private readonly IReplyService _replyService;
        private readonly IHubContext<NotificationHub> _hubContext;

        public ReplyController(IReplyService replyService,IHubContext<NotificationHub> hubContext)
        {
            _replyService = replyService;
            _hubContext = hubContext;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var response = _replyService.GetAll();
            return Ok(response);
        }

        [HttpGet("{id}")]
        public IActionResult GetOne(long id)
        {
            var response = _replyService.GetOne(id);
            if (response == null)
                return NotFound();
            return Ok(response);
        }

        [HttpGet("post/{postId}")]
        public IActionResult GetByPost(long postId)
        {
            var response = _replyService.GetByPostId(postId);
            if (response == null)
                return NotFound();
            return Ok(response);
        }
        [HttpPost]
        public IActionResult Add([FromBody] ReplyDto dto)
        {
            if (dto == null)
                return BadRequest();
            var response = _replyService.Add(dto.ToEntity());
            if (response.IsSuccess)
                _hubContext.Clients.Group(response.Data.PostId.ToString()).SendAsync("ReplyAdded", $"{response.Data.User.Username} replied on your post about {response.Data.Post.Title}");
            return Created("localhost:8080/api/replies/" + response.Data?.Id, response);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] ReplyDto dto)
        {
            if (dto == null)
                return BadRequest();
            var response = _replyService.Update(dto.ToEntity());
            if (response.IsSuccess)
                _hubContext.Clients.Group(response.Data.PostId.ToString()).SendAsync("ReplyChanged", $"{response.Data.User.Username} changed his reply for {response.Data.Post.Title}");
            return Ok(response);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var response = _replyService.GetOne(id);
            if (response == null)
                return BadRequest();
            _replyService.Delete(id);
            return Ok();
        }
    }
}