using FTNForum.Services.Interface;
using FTNForum.Services.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FTNForum.Web.Controllers
{
    [ApiController]
    [Route("api/utils")]
    public class UtilController : ControllerBase
    {
        private readonly IUtilService _utilService;

        public UtilController(IUtilService utilService)
        {
            _utilService = utilService;
        }
        [HttpPost("uploadFile")]
        public IActionResult UploadImage(IFormCollection formCollection)
        {

            var path = _utilService.UploadFile(formCollection);

            return Ok(path);

        }
    }
}