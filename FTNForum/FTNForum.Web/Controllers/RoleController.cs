using FTNForum.Services.DTO;
using FTNForum.Services.Interface;
using FTNForum.Services.Utils;
using Microsoft.AspNetCore.Mvc;

namespace FTNForum.Web.Controllers
{
    [ApiController]
    [Route("api/roles")]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _roleService;

        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var entityList = _roleService.GetAll();
            return Ok(EntityConverter.ToDTOs(entityList));
        }

        [HttpGet("{id}")]
        public IActionResult GetOne(long id)
        {
            var entity = _roleService.GetOne(id);
            if (entity == null)
                return NotFound();
            return Ok(EntityConverter.ToDTO(entity));
        }

        [HttpPost]
        public IActionResult Add([FromBody] RoleDto dto)
        {
            if (dto == null)
                return BadRequest();
            var entity = _roleService.Add(EntityConverter.ToEntity(dto));
            return Created("localhost:8080/api/roles/" + entity.Id, EntityConverter.ToDTO(entity));
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] RoleDto dto)
        {
            if (_roleService.GetOne(id) == null)
                return BadRequest();
            var entity = _roleService.Update(EntityConverter.ToEntity(dto));
            return Ok(EntityConverter.ToDTO(entity));
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var forum = _roleService.GetOne(id);
            if (forum == null)
                return BadRequest();
            _roleService.Delete(id);
            return Ok();
        }
    }
}