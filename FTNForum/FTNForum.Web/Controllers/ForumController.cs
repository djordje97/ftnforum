using FTNForum.Services.DTO;
using FTNForum.Services.Interface;
using FTNForum.Services.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FTNForum.Web.Controllers
{
    [ApiController]
    [Route("api/forums")]
    public class ForumController : ControllerBase
    {
        private readonly IForumService _forumService;
        private readonly IUtilService _utilService;

        public ForumController(IForumService forumService,IUtilService utilService)
        {
            _forumService = forumService;
            _utilService = utilService;
        }

        [HttpGet]
        public IActionResult GetAll([FromQuery] PaginationQuery request)
        {
            var response = _forumService.GetAll(request);
            return Ok(response);
        }

        [HttpGet("{id}")]
        public IActionResult GetOne(long id)
        {
            var response = _forumService.GetOne(id);
            if (response.Data == null)
                return NotFound(response);
            return Ok(response);
        }

        [HttpPost]
        public IActionResult Add([FromBody] ForumDto dto)
        {
            if (dto == null)
                return BadRequest();
            var response = _forumService.Add(dto.ToEntity());
            return Created("localhost:8080/api/forums/" + response.Data?.Id, response);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] ForumDto dto)
        {
            if (dto == null)
                return BadRequest();
            var response = _forumService.Update(dto.ToEntity());
            return Ok(response);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var forum = _forumService.GetOne(id);
            if (forum == null)
                return BadRequest();
            _forumService.Delete(id);
            return Ok();
        }
    }
}