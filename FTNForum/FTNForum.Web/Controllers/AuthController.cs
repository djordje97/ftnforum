using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using FTNForum.Repositories.Interface;
using FTNForum.Services.DTO;
using FTNForum.Services.Interface;
using FTNForum.Services.Utils;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace FTNForum.Web.Controllers
{
    [ApiController]
    [Route("api/auth")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly IConfiguration _configuration;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost, Route("login")]
        public IActionResult Login([FromBody] LoginDto request)
        {
            var loginResult = _authService.Login(request);
            return Ok(loginResult);

        }
        
        [HttpPost, Route("changePassword")]
        public IActionResult ChangePassword([FromBody] ChangePasswordDto request)
        {
            var response = _authService.ChangePassword(request);
            return Ok(response);

        }
    }
}