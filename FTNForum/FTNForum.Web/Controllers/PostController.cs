using FTNForum.Services.DTO;
using FTNForum.Services.Interface;
using FTNForum.Services.Utils;
using FTNForum.Web.utils;
using Microsoft.AspNetCore.Mvc;

namespace FTNForum.Web.Controllers
{
    [ApiController]
    [Route(ApiRoutes.Posts.PostsApi)]
    public class PostController : ControllerBase
    {
        private readonly IPostService _postService;

        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var response = _postService.GetAll();
            return Ok(response);
        }

        [HttpGet("latest")]
        public IActionResult GetLatest()
        {
            var response = _postService.GetLatest();
            return Ok(response);
        }

        [HttpGet("forum")]
        public IActionResult GetByForumId([FromQuery] PaginationQuery request)
        {
            var response = _postService.GetByForumId(request);
            return Ok(response);
        }
        [HttpGet("search")]
        public IActionResult SearchPosts([FromQuery]PaginationQuery reqestParsed)
        {
            var response = _postService.SearchPosts(reqestParsed);
            return Ok(response);
        }

        [HttpGet("{id}")]
        public IActionResult GetOne(long id)
        {
            var response = _postService.GetOne(id);
            if (response.Data == null)
                return NotFound();
            return Ok(response);
        }

        [HttpPost]
        public IActionResult Add([FromBody] PostDto dto)
        {
            if (dto == null)
                return BadRequest();
            var response = _postService.Add(dto.ToEntity());
            return Created("localhost:8080/api/posts/" + response.Data?.Id, response);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] PostDto dto)
        {
            if (dto == null)
                return BadRequest();
            var response = _postService.Update(dto.ToEntity());
            return Ok(response);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var post = _postService.GetOne(id);
            if (post == null)
                return BadRequest();
            _postService.Delete(id);
            return Ok();
        }
    }
}