using FTNForum.Services.DTO;
using FTNForum.Services.Interface;
using FTNForum.Services.Utils;
using FTNForum.Web.utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FTNForum.Web.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Authorize]
        public IActionResult GetAll([FromQuery] PaginationQuery request)
        {
            var response = _userService.GetAll(request);
            return Ok(response);
        }

        [HttpGet("{id}")]
        public IActionResult GetOne(long id)
        {
            var response = _userService.GetOne(id);
            if (response.Data == null)
                return NotFound();
            return Ok(response);
        }

        [ValidateModel]
        [HttpPost]
        public IActionResult Add([FromBody] UserDto dto)
        {
            var hasher = new PasswordHasher<LoginDto>();
            if (dto == null)
                return BadRequest();
            dto.Password = hasher.HashPassword(null,dto.Password);
            
            var response = _userService.Add(dto.ToEntity());
            return Created("localhost:8080/api/users/" + response.Data?.Id, response);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] UserDto dto)
        {
            if (dto == null)
                return BadRequest();
            var response = _userService.Update(dto.ToEntity());
            return Ok(response);
        }
        
        [HttpPut("promote/{id}")]
        public IActionResult Promote(long id, [FromBody] UserDto dto)
        {
            if (dto == null)
                return BadRequest();
            var response = _userService.PromoteUserToAdmin(dto.ToEntity());
            return Ok(response);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var forum = _userService.GetOne(id);
            if (forum == null)
                return BadRequest();
            _userService.Delete(id);
            return Ok();
        }
    }
}