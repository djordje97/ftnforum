using System.Threading.Tasks;
using FTNForum.Services.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace FTNForum.Web.Hubs
{
    public class NotificationHub : Hub
    {

        public async Task JoinQuestionGroup(long questionId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, questionId.ToString());
        }
    }
}