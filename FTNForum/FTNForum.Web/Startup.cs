using System.Text;
using FTNForum.Repositories;
using FTNForum.Repositories.Interface;
using FTNForum.Repositories.Repository;
using FTNForum.Services.Interface;
using FTNForum.Services.Service;
using FTNForum.Web.Hubs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;

namespace FTNForum.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ApiBehaviorOptions>(opt =>
            {
                opt.SuppressModelStateInvalidFilter = true;
            });
            services.AddMvc(opt => opt.EnableEndpointRouting = false);
            services.AddDbContext<ForumDbContext>(options =>
                options.UseLazyLoadingProxies()
                .UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
                    o=> o.MigrationsAssembly("FTNForum.Repositories")
                    )
            );
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(build =>
                build.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.GetValue<string>("JWT:Secret")))
                }
            );
            //Repositories
            services.AddScoped<ForumDbContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IUnitOfWork,UnitOfWork>();
            services.AddScoped<IForumRepository,ForumRepository>();
            services.AddScoped<IPostRepository,PostRepository>();
            services.AddScoped<IReplyRepository,ReplyRepository>();
            services.AddScoped<ILikeRepository,LikeRepository>();
            services.AddScoped<IUserRepository,UserRepository>();
            services.AddScoped<IRoleRepository,RoleRepository>();
            //Services
            services.AddScoped<IForumService,ForumService>();
            services.AddScoped<IPostService, PostService>();
            services.AddScoped<IReplyService, ReplyService>();
            services.AddScoped<ILikeService, LikeService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IUtilService, UtilService>();

            services.AddHttpContextAccessor();
            
            services.AddControllers();
            services.AddOptions();
            services.AddSignalR();

        }
        
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSignalR(conf => conf.MapHub<NotificationHub>("/api/notificationHub"));
            app.UseCors(builder => builder.WithOrigins("http://localhost:8080").AllowAnyHeader().AllowAnyMethod());
            app.UseAuthentication();    
            app.UseMvc();
            
        }
    }
}