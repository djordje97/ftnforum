namespace FTNForum.Web.utils
{
    public class ApiRoutes
    {
        public const string Root = "api";

        public static class Posts
        {
            public const string PostsApi = Root+"/posts";
            public const string Latest ="latest";
            public const string GetAll = PostsApi;

        }
        

    }
}