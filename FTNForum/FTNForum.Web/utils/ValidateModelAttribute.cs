using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace FTNForum.Web.utils
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var errors = context.ModelState
                    .Where(a => a.Value.Errors.Count > 0)
                    .Select(x => x.Value.Errors.FirstOrDefault().ErrorMessage)
                    .ToList();
                var errorMessage = "";
                foreach (var error in errors)
                {
                    errorMessage += error+' ';
                }
                
                context.Result = new BadRequestObjectResult(errorMessage);
                return;
            }

            base.OnActionExecuting(context);
        }
    }
}