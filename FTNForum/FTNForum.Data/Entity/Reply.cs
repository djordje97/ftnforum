using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FTNForum.Data.Util;

namespace FTNForum.Data.Entity
{
    public class Reply :IBaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public long PostId { get; set; }
        public long UserId { get; set; }
        public virtual Post Post { get; set; }
        public virtual User User{ get; set; }
        public virtual List<Like> Likes { get; set; }

        public Reply()
        {
            Likes=new List<Like>();
        }
    }
}