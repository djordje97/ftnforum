using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FTNForum.Data.Util;

namespace FTNForum.Data.Entity
{
    public class Post :IBaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public long UserId { get; set; }
        public long ForumId { get; set; }
        public virtual User User { get; set; }
        public virtual Forum Forum{ get; set; }
        public virtual  List<Reply> Replies { get; private set; }

        public Post()
        {
            Replies=new List<Reply>();
        }
    }
}