using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime;
using FTNForum.Data.Util;

namespace FTNForum.Data.Entity
{
    public class User :IBaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public string ImageUrl { get; set; }        
        public DateTime CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public long RoleId { get; set; }
        
        public virtual Role Role { get; set; }
        public virtual List<Post> Posts { get; private set; }
        public virtual List<Like> Likes { get; private set; }
        public virtual List<Reply> Replies { get; private set; }

        public User()
        {
            Posts=new List<Post>();
            Likes=new List<Like>();
            Replies=new List<Reply>();
        }
    }
}