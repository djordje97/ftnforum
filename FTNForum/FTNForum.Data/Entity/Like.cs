using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FTNForum.Data.Util;

namespace FTNForum.Data.Entity
{
    public class Like : IBaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public bool IsLike { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public long ReplyId { get; set; }
        public long UserId { get; set; }

        public virtual Reply Reply { get; set; }
        public virtual User User { get; set; }
    }
}