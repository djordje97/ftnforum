using System;

namespace FTNForum.Data.Util
{
    public interface IBaseEntity
    {
        public DateTime CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
    }
}