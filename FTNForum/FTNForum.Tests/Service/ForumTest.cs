﻿using FTNForum.Data.Entity;
using FTNForum.Repositories.Interface;
using FTNForum.Services.DTO;
using FTNForum.Services.Service;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace FTNForum.Tests.Service
{
    public class ForumTest
    {
        [Fact]
        public void Should_Return_Forum_List()
        {
            var mock = new Mock<IUnitOfWork>();
            mock.Setup(p => p.Forums.GetAll()).Returns(new List<Forum>
            {
                new Forum {Id = 1, Title = "C#", Description = "Some Description"},
                new Forum {Id = 2, Title = "Java", Description = "Some Description"},
                new Forum {Id = 3, Title = "JavaScript", Description = "Some Description"}
            });

            ForumService service = new ForumService(mock.Object);
            // var forums = service.GetAll();
            // Assert.Equal(3, forums.Data.Count);
            // Assert.NotEmpty(forums.Data);

        }

        [Fact]
        public void Should_Return_Forum_By_Id()
        {
            var mock = new Mock<IUnitOfWork>();
            mock.Setup(p => p.Forums.GetById(1)).Returns(
                new Forum {Id = 1, Title = "C#", Description = "Some Description"});

            ForumService service = new ForumService(mock.Object);
            var forum = service.GetOne(1);
            Assert.NotNull(forum);
            Assert.True(forum.Data.Id == 1);

        }
    }
}
//
//         [Fact]
//         public void Should_Add_Forum()
//         {
//             Forum newForum = new Forum()
//             {
//                 Id = 2,
//                 Title = "Java",
//                 Description = "Something"
//             };
//             var mock = new Mock<IUnitOfWork>();
//             mock.Setup(p => p.Forums.Add(newForum)).Returns(
//               new Forum() { Id = 2, Title = "Java", Description = "Something" }
//               );
//             mock.Setup(p => p.Forums.GetAll()).Returns(new List<Forum>()
//             {
//                 new Forum {Id=1,Title="C#",Description = "Some Description" },
//                 new Forum { Id = 2, Title = "Java", Description = "Something" }
//             });
//             Forum newForumDto = new Forum()
//             {
//                 Id=2,
//                 Title = "Java",
//                 Description = "Something"
//             };
//             ForumService service = new ForumService(mock.Object);
//             service.Add(newForumDto);
//             var forums = service.GetAll();
//             Assert.Equal(2, forums.Count);
//
//
//         }
//     }
// }
